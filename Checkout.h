#pragma once
#include "Supply.h"
#include <vector>
#include <string>
#include <map>


class FileReader;

class Checkout
{
public:
	Checkout();
	virtual ~Checkout();

	void addItemPrice(std::string item, int price);
	void addItem(std::string item);
	void addDiscount(std::string item, int nbrOfItems, int discountPrice, bool isBulk);
	void addCoupon(std::string, float rate);
	int calculateTotal();

	std::string userCode;


protected:
	struct Discount {
		int nbrOfItems;
		int discountPrice;
		bool isBulk;
	};

	std::map<std::string, int> items;
	std::map<std::string, int> prices;	
	std::map<std::string, Discount> discounts;
	std::map<std::string, float> coupons;
		
	int total;
	

	void calculateItem(std::string item, int itemCnt);
	void calculateDiscount(std::string item, int itemCnt, Discount discount);
};

