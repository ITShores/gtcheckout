#include "Checkout.h"
#include "Supply.h"
#include <stdexcept>

using namespace std;

Checkout::Checkout() :total(0)
{
}

Checkout::~Checkout()
{
}

//Adds item and price to price map
void Checkout::addItemPrice(std::string item, int price) {
	prices[item] = price;
}

//Adds item from price map to item map
//and throws error if item has no price
void Checkout::addItem(std::string item) {
	std::map<std::string, int>::iterator priceIter = prices.find(item);
	if (priceIter == prices.end()) {
		throw std::invalid_argument("Invalid item. No price");
	}

	items[item]++;
}

//Adds discount item to discount struct
void Checkout::addDiscount(std::string item, int nbrOfItems, int discountPrice, bool isBulk) {
	Discount discount;
	discount.nbrOfItems = nbrOfItems;
	discount.discountPrice = discountPrice;
	discount.isBulk = isBulk;
	discounts[item] = discount;
	
}

//Adds discount rate to coupon code
void Checkout::addCoupon(std::string couponCode, float rate) {
	coupons[couponCode] = rate;
}
//Iterates through item map and coupon map to calculate total
int Checkout::calculateTotal() {
	total = 0;

	for (std::map<std::string, int>::iterator itemIter = items.begin(); itemIter != items.end(); itemIter++) {
		std::string item = itemIter->first;
		int itemCnt = itemIter->second;
		calculateItem(item, itemCnt);
	}
	for (std::map<std::string, float>::iterator couponIter = coupons.begin(); couponIter != coupons.end(); couponIter++) {
		std::string coupon = couponIter->first;
		float rate = couponIter->second;
		if (coupon == userCode) {
			total = total * rate;
		}
	}
	
	return total;
}

//Changes item price based on discount
void Checkout::calculateItem(std::string item, int itemCnt) {
	std::map<std::string, Discount>::iterator discountIter;
	discountIter = discounts.find(item);
	if (discountIter != discounts.end()) {
		Discount discount = discountIter->second;
		calculateDiscount(item, itemCnt, discount);
	}
	else {
		total += itemCnt * prices[item];
	}
}

//Adds discounted items to total
void Checkout::calculateDiscount(std::string item, int itemCnt, Discount discount) {
	if (discount.isBulk == true && itemCnt >= discount.nbrOfItems) {
		int nbrOfDiscounts = itemCnt - discount.nbrOfItems;
		total += nbrOfDiscounts * discount.discountPrice;
		total += discount.nbrOfItems * prices[item];
	}
	else if (itemCnt >= discount.nbrOfItems) {
		int nbrOfDiscounts = itemCnt / discount.nbrOfItems;
		total += nbrOfDiscounts * discount.discountPrice;
		int remainingItems = itemCnt % discount.nbrOfItems;
		total += remainingItems * prices[item];		
	}
	else {
		total += itemCnt * prices[item];
	}
}