#include <iostream>
#include <fstream>
#include <gtest/gtest.h>
#include "Checkout.h"
#include "Supply.h"


class CheckoutTests : public ::testing::Test {
public:
	
protected:
	Checkout checkOut;
	FileReader supplyList;
};

//Checks if Checkout can add an item with price to total
TEST_F(CheckoutTests, CanCalculateTotal) {
	checkOut.addItemPrice("a", 1);
	checkOut.addItem("a");
	int total = checkOut.calculateTotal();
	ASSERT_EQ(1, total);
}

//Checks if Checkout can add multiple items with prices to total
TEST_F(CheckoutTests, CanGetTotalForMultipleItems) {
	checkOut.addItemPrice("a", 1);
	checkOut.addItemPrice("b", 2);
	checkOut.addItem("a");
	checkOut.addItem("b");
	int total = checkOut.calculateTotal();
	ASSERT_EQ(3, total);
}

TEST_F(CheckoutTests, CanAddDiscount) {
	checkOut.addDiscount("a", 3, 2, false);
}

//Checks if total includes discounted item prices properly
TEST_F(CheckoutTests, CanCalculateTotalWithDiscount) {
	checkOut.addItemPrice("a", 1);
	checkOut.addDiscount("a", 3, 2, false);
	checkOut.addItem("a");
	checkOut.addItem("a");
	checkOut.addItem("a");
	int total = checkOut.calculateTotal();
	ASSERT_EQ(2, total);
}

TEST_F(CheckoutTests, ItemWithNoPriceThrowsException) {
	ASSERT_THROW(checkOut.addItem("a"), std::invalid_argument);
}

//Checks if checkout class can calculate total
//from txt input file of six items with prices 3 2 2 4 4 and 1
TEST_F(CheckoutTests, CanLoadTxt) {
	supplyList.addSupplyToInventory("../supply.txt");
	supplyList.addToCheckout(checkOut);
	int total = checkOut.calculateTotal();
	ASSERT_EQ(16, total);
}

//Checks if total is correctly calculated
//with 75% discount on item "a" after 2 are bought
TEST_F(CheckoutTests, CanCalculateTotalWithBulkDiscount) {
	checkOut.addItemPrice("a", 4);
	checkOut.addDiscount("a", 2, 1, true);
	checkOut.addItem("a");
	checkOut.addItem("a");
	checkOut.addItem("a");
	checkOut.addItem("a");
	checkOut.addItem("a");
	checkOut.addItem("a");
	int total = checkOut.calculateTotal();
	ASSERT_EQ(12, total);
}

//Checks if total is correctly calculated
//with 50% discount on candy after 2 are bought
//file also includes 3 other items with prices 3 3 and 2
TEST_F(CheckoutTests, CanUseBulkDiscountWithFile) {
	supplyList.addSupplyToInventory("../supplyDiscount.txt");
	supplyList.addToCheckout(checkOut);
	checkOut.addDiscount("candy", 2, 1, true);
	int total = checkOut.calculateTotal();
	ASSERT_EQ(15, total);
}

//Checks if checkout class can pass a code
//to add discount of 20% on final total
TEST_F(CheckoutTests, CanUseCode) {
	checkOut.addCoupon("SPECIAL20", 0.8);
	checkOut.addItemPrice("a", 5);
	checkOut.addItem("a");
	checkOut.addItem("a");
	checkOut.addItem("a");
	checkOut.userCode = "SPECIAL20";
	int total = checkOut.calculateTotal();
	ASSERT_EQ(12, total);
}

//Checks if total can be calculated
//with bulk discount and coupon
TEST_F(CheckoutTests, CanCalculateTotalWithBulkAndCoupon) {
	supplyList.addSupplyToInventory("../supplyDiscount.txt");
	supplyList.addToCheckout(checkOut);
	checkOut.addCoupon("SPECIAL20", 0.8);
	checkOut.addDiscount("candy", 2, 1, true);
	checkOut.userCode = "SPECIAL20";
	int total = checkOut.calculateTotal();
	ASSERT_EQ(12, total);
}