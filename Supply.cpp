#include "Supply.h"
#include "Checkout.h"
#include <stdexcept>


using namespace std;


FileReader::FileReader():total(0)
{	
}

FileReader::~FileReader()
{
}

//Checks the end characters of given file path string 
//against predetermined file type string
//will also return true if file type is empty
bool FileReader::checkFile(const std::string filePath) {
		return filePath.size() >= fileType.size() &&
			filePath.compare(filePath.size() - fileType.size(), fileType.size(), fileType) == 0;
}

//Checks if file can be opened
void FileReader::getFile(const std::string filePath) {
	if (checkFile(filePath) == true) {
		fstream supplyFile;
		supplyFile.open(filePath);
		canOpen = !supplyFile.fail();
		supplyFile.close();
	}
}

//Updates inventory struct with items and prices from file
int FileReader::addSupplyToInventory(const std::string filePath) {
	fstream supplyFile;
	getFile(filePath);
	if (canOpen == true) {
		supplyFile.open(filePath);
		while (!supplyFile.eof()) {
			inventoryItem currentItem{};
			supplyFile >> currentItem.name >> currentItem.price;
			inventory.push_back(currentItem);
		}
		supplyFile.close();
	}
	return inventory.size();
}

//Passes inventory items to checkout 
void FileReader::addToCheckout(Checkout& checkout) {
	for (auto& item : inventory) {
		checkout.addItemPrice(item.name, item.price);
		checkout.addItem(item.name);
	}
}