#pragma once
#include "Checkout.h"
#include <iostream>
#include <fstream>
#include <vector>

class Checkout;

struct inventoryItem {
	std::string name;
	int price;
};

class FileReader
{
public:
	FileReader();
	virtual ~FileReader();

	bool canOpen = false;
	std::string fileType;

	bool checkFile(const std::string filePath);
	void getFile(const std::string filePath);
	int addSupplyToInventory(const std::string filePath);
	void addToCheckout(Checkout& checkout);
	
	

protected:
	
	int total;
	std::vector<inventoryItem> inventory;
};

