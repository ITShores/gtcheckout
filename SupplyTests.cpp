#include "Checkout.h"
#include "Supply.h"
#include <iostream>
#include <fstream>
#include <gtest/gtest.h>



class FileReaderTests : public ::testing::Test {
public:
	
	Checkout checkOut;
protected:


	FileReader supplyList;

};

//Checks file against file type
// and if file can be opened
TEST_F(FileReaderTests, CanOpenTxt) {
	supplyList.fileType = ".txt";
	supplyList.getFile("../supply.txt");
	ASSERT_EQ(supplyList.canOpen, true);
}

//Checks if all items in file are added to inventory
TEST_F(FileReaderTests, CanAddToInventory) {
	int invCount = supplyList.addSupplyToInventory("../supply.txt");
	ASSERT_EQ(invCount, 6);
}